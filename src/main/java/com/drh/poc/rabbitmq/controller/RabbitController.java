package com.drh.poc.rabbitmq.controller;

import com.drh.poc.rabbitmq.producer.RabbitProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RabbitController {

  private final RabbitProducer producer;

  @PostMapping("/send")
  public void sendMessage(@RequestBody String message) {
    producer.sendMessage("myQueue", message);
  }
}
