package com.drh.poc.rabbitmq.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitConsumer {

  @Value("${rabbit.queue}")
  private String queue;

  @RabbitListener(queues = "${rabbit.queue}")
  public void myQueueListener(Message message) {
    log.info("Message read from {} : {}", queue, new String(message.getBody()));
//    throw new RuntimeException("test dlx");
  }
}
